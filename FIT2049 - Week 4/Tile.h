/*	FIT2096 - Assignment 1 Sample Solution
*	Tile.h
*	Created by Mike Yeates - 2017 - Monash University
*	A Tile represents a coloured cell on the board.
*/

#ifndef TILE_H
#define TILE_H

#include "GameObject.h"
#include "TextureManager.h"
#include "Collisions.h"

// Define all the types of tiles we could be (naming these by function instead of appearance).
enum class TileType
{
	HEALTH,
	DAMAGE,
	TELEPORT,
	NORMAL,
	DISABLED,
	MONSTER_VAR1,
	MONSTER_VAR2,
	WALL,
	INVALID // Used if we query a tile which doesn't exist
};

class Tile : public GameObject
{
private:
	TileType m_type;
	TextureManager* m_textureManager;

	// We'll be animating so a second position is required
	Vector3 m_targetPosition;
	float m_timeUntilMove;
	float m_moveSpeed;

	TileType SelectType();
	Texture* GetTextureForType(TileType type);

	bool m_doRender; //if the tile will be rendered (only used for disabling health packs)
	bool m_3dModel; //if the tile is a 3d model, used to ensure only they spin

	// Collisions
	CBoundingBox m_boundingBox;

public:
	Tile();
	Tile(Mesh* mesh, Shader* shader, Vector3 pos, TextureManager* textureManager);
	Tile(Mesh* mesh, Shader* shader, Vector3 pos, TextureManager* textureManager, TileType type);
	Tile(Mesh* mesh, Shader* shader, Vector3 pos, Texture* texture, TileType type); //For floating 3d models
	~Tile();

	void Update(float timestep);

	// Instruct a tile to start falling from a specified height
	void DropFromHeight(float dropHeight, float stopHeight, float speed, float delay);

	TileType GetType() { return m_type; }
	bool GetDoRender() { return m_doRender; }
	bool Get3dModel() { return m_3dModel; }
	CBoundingBox GetBounds() { return m_boundingBox; } // returns bounding box, for collisions
	
	void SetType(TileType type);
	void SetDoRender(bool render) { m_doRender = render; }

};

#endif

