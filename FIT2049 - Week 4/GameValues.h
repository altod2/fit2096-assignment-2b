#ifndef GAMEVALUES_H
#define GAMEVALUES_H

//Tweakable games values appear in this file


//Player values
//------------------------------------------------------------------------
//------------------------------------------------------------------------

//General
const float playerHealth = 100.0f;

//Damage
const int playerDamageMax = 25; //min and max damage the players bullets do
const int playerDamageMin = 15;
const int playerFireRate = 30; //rate at which players's gun fires (higher is slower)

//Movement
const float playerMoveSpeed = 0.005f;
const float playerFriction = 0.08f;

//Monster values
//------------------------------------------------------------------------
//------------------------------------------------------------------------

//General
const int monsterHealthMax = 100; //max and min health amounts for the monsters
const int monsterHealthMin = 50;

//Damage
const int monsterDamageMax = 5; //min and max damage the players bullets do
const int monsterDamageMin = 1;
const int monsterFireRate = 200; //rate at which monster's gun fires (higher is slower)

//Movement
const float monsterBaseMoveSpeed = 0.001f; //Final move speed = base move speed * monster skill
const float monsterFriction = 0.08f;
const float monsterHeadOffDist = 15.0f; //how far away the monster that heads off the player will be (multiplier)
const float monsterScareRange = 10.0f; //how close the player can get to movement type 5 enemies before they move

//General values
//------------------------------------------------------------------------
//------------------------------------------------------------------------

const int monsterCount = 5;
const float bulletMoveSpeed = 0.5f;
const float bulletTravelDist = 100.0f; //How far bullets will go from their origin before they disappear

#endif
