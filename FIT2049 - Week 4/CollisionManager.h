#ifndef COLLISION_MANAGER_H
#define COLLISION_MANAGER_H

#include <vector>
#include "Collisions.h"
#include "Player.h"
#include "Monster.h"
#include "Tile.h"
#include "Bullet.h"

#define MAX_ALLOWED_COLLISIONS 2048

class CollisionManager
{
private:
	std::vector<Tile*>* m_tiles;
	std::vector<Monster*>* m_monsters;
	std::vector<Player*>* m_players; // Vector of players so it is expandable
	std::vector<Bullet*>* m_playerBullets;
	std::vector<Bullet*>* m_monsterBullets;

	GameObject* m_currentCollisions[MAX_ALLOWED_COLLISIONS];

	// We need to know what objects were colliding last frame so we can determine if a collision has just begun or ended
	GameObject* m_previousCollisions[MAX_ALLOWED_COLLISIONS];

	int m_nextCurrentCollisionSlot;

	// Check if we already know about two objects colliding
	bool ArrayContainsCollision(GameObject* arrayToSearch[], GameObject* first, GameObject* second);

	// Register that a collision has occurred
	void AddCollision(GameObject* first, GameObject* second);

	// Collision check helpers
	void PlayerToTile();
	void MonsterToTile();
	void PlayerToMonster();
	void BulletToMonster();
	void BulletToPlayer();

public:
	CollisionManager(std::vector<Tile*>* walls, std::vector<Monster*>* monsters, std::vector<Player*>* players, std::vector<Bullet*>* playerBullets, std::vector<Bullet*>* monsterBullets);
	void CheckCollisions();

};

#endif