#include "FirstPersonCamera.h"
#include "MathsHelper.h"


FirstPersonCamera::FirstPersonCamera()
{
}

FirstPersonCamera::FirstPersonCamera(InputController* input, GameObject* player, bool catchup, float catchupSpeed)
{
	m_input = input;
	m_playerObject = player;
	m_catchupMode = catchup;
	m_catchupSpeed = catchupSpeed;
	
	m_rotationSpeed = 0.4f;

	m_heading = 0.0f;
	m_pitch = 0.0f;

	SetPosition(m_playerObject->GetPosition() + Vector3(0.0f, 2.0f, 0.0f));
}

void FirstPersonCamera::Update(float timestep)
{
	if (m_playerObject != NULL)
	{
		// We'll look at the object we're following
		// Sometimes we'll even add an offset here too 
		//SetLookAt(m_playerObject->GetPosition());

		if (m_catchupMode)
		{
			// Catchup to target position. A simple LERP will do.
			SetPosition(Vector3::Lerp(GetPosition(), m_playerObject->GetPosition() + Vector3(0.0f, 2.0f, 0.0f), timestep * m_catchupSpeed));
		}
		else
		{
			// Jump directly to target position (including offset)
			SetPosition(m_playerObject->GetPosition());
		}
	}
	// Accumulate change in mouse position 
	m_heading += m_input->GetMouseDeltaX() * m_rotationSpeed * timestep;
	m_pitch += m_input->GetMouseDeltaY() * m_rotationSpeed * timestep;

	// Limit how far the player can look down and up
	m_pitch = MathsHelper::Clamp(m_pitch, ToRadians(-80.0f), ToRadians(80.0f));

	// Wrap heading and pitch up in a matrix so we can transform our look at vector
	// Heading is controlled by MouseX (horizontal movement) but it is a rotation around Y
	// Pitch  controlled by MouseY (vertical movement) but it is a rotation around X
	Matrix heading = Matrix::CreateRotationY(m_heading);
	Matrix pitch = Matrix::CreateRotationX(m_pitch);

	// We're going to need this a lot. Store it locally here to save on our function calls 
	Vector3 currentPos = GetPosition();

	// Combine pitch and heading into one matrix for convenience
	Matrix lookAtRotation = pitch * heading;

	// Transform a world forward vector into local space (take pitch and heading into account)
	Vector3 lookAt = Vector3::TransformNormal(Vector3(0, 0, 1), lookAtRotation);

	// At this point, our look-at vector is still relative to the origin
	// Add our position to it so it originates from the camera and points slightly in front of it
	// Remember the look-at vector needs to describe a point in the world relative to the origin
	lookAt += currentPos;

	// Use parent's mutators so isDirty flags get flipped
	SetLookAt(lookAt);
	SetPosition(currentPos);

	// Give our parent a chance to regenerate the view matrix
	Camera::Update(timestep);
}


