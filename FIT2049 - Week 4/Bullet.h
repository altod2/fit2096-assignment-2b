#ifndef BULLET_H
#define BULLET_H

#include "GameObject.h"
#include "Collisions.h"
#include "GameValues.h"

class Bullet :
	public GameObject
{
private:

	float m_moveSpeed;
	bool m_hostile; // if the bullet is dangerous to the player (true if yes)
	bool m_enabled; // disable updating and rendering if false
	Vector3 m_targetPosition; // postion bullet is moving to
	Vector3 m_localForward; // direction the bullet is headed
	float m_travelDistance; // how far a bullet will go before disappearing
	
	// Collisions
	CBoundingBox m_boundingBox;
public:
	Bullet(Mesh* mesh, Shader* shader, Texture* texture, bool hostile, Vector3 startPos, Vector3 localForward, float rotY);
	~Bullet();

	void Update(float timestep); //so the class has an override for the virtual update function of gameobject
	void Update(float timestep, Vector3 shooterPosition);

	// Hitting a monster
	void OnMonsterCollisionEnter();
	void OnMonsterCollisionStay();
	void OnMonsterCollisionExit();

	// Hitting a player
	void OnPlayerCollisionEnter();
	void OnPlayerCollisionStay();
	void OnPlayerCollisionExit();

	//Accessors
	bool GetHostile() { return m_hostile; }
	bool GetEnabled() { return m_enabled; }
	CBoundingBox GetBounds() { return m_boundingBox; } // returns bounding box, for collisions

	//Mutators
	void SetEnabled(bool enabled) { m_enabled = enabled; }
};

#endif


