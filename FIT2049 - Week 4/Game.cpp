/*	FIT2096 - Assignment 1 Sample Solution
*	Game.h
*	Created by Mike Yeates - 2017 - Monash University
*/

#include "Game.h"
#include "TexturedShader.h"
#include "StaticObject.h"
#include "DirectXTK/CommonStates.h"
#include <sstream>

Game::Game()
{
	m_renderer = NULL;
	m_currentCam = NULL;
	m_input = NULL;
	m_meshManager = NULL;
	m_textureManager = NULL;
	m_diffuseTexturedShader = NULL;
	m_gameBoard = NULL;
	m_movesRemainingBar = NULL;
	m_player = NULL;
	m_spriteBatch = NULL;
	m_font = NULL;
	m_fontLarge = NULL;
	m_button = NULL;
}

Game::~Game() 
{
	//Causes a read access violation error, not sure how to fix and delete these properly
	/*
	for (int i = 0; i < m_tiles.size(); i++)
	{
		delete m_tiles[i];
		m_tiles[i] = NULL;
	}
	for (int i = 0; i < m_players.size(); i++)
	{
		delete m_players[i];
		m_players[i] = NULL;
	}
	for (int i = 0; i < m_monsters.size(); i++)
	{
		delete m_monsters[i];
		m_monsters[i] = NULL;
	}
	for (int i = 0; i < m_monsterBullets.size(); i++)
	{
		delete m_monsterBullets[i];
		m_monsterBullets[i] = NULL;
	}
	
	delete m_playerBullets;
	m_playerBullets = NULL;
	*/
}

bool Game::Initialise(Direct3D* renderer, InputController* input)
{
	m_renderer = renderer;	
	m_input = input;
	m_meshManager = new MeshManager();
	m_textureManager = new TextureManager();

	if (!InitShaders())
		return false;

	if (!LoadMeshes())
		return false;

	if (!LoadTextures())
		return false;
	
	LoadFonts();
	InitUI();
	InitGameWorld();
	RefreshUI();

	//CollisionManager(std::vector<Tile*>* walls, std::vector<Monster*>* monsters, std::vector<Player*>* players);
	m_tiles = m_gameBoard->GetTilesVector(); //store tiles vector so we can pass the address
	m_playerBullets = m_player->GetBulletsVector();
	m_players.push_back(m_player); //add player object to a vector
	m_collisionManager = new CollisionManager(&m_tiles, &m_monsters, &m_players, m_playerBullets, &m_monsterBullets);
	//m_currentCam = new Camera();
	m_currentCam = new FirstPersonCamera(m_input, m_player, true, 3.0f);

	return true;
}

bool Game::InitShaders()
{
	m_diffuseTexturedShader = new TexturedShader();
	if (!m_diffuseTexturedShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/TexturedPixelShader.ps"))
		return false;

	return true;
}

bool Game::LoadMeshes()
{
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/floor_tile.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/wall_tile.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/player_capsule.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/enemy.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/bullet.obj"))
		return false;

	return true;
}

bool Game::LoadTextures()
{
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_blue.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_disabled.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_green.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_orange.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_purple.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_red.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_white.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/sprite_exit.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/sprite_healthBar.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/bullet.png"))
		return false;

	return true;
}

void Game::LoadFonts()
{
	m_font = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-12pt.spritefont");
	m_fontLarge = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-18pt.spritefont");
}

void Game::InitUI()
{
	m_spriteBatch = new SpriteBatch(m_renderer->GetDeviceContext());

	m_button = new Button(128, 128, m_textureManager->GetTexture("Assets/Textures/sprite_exit.png"), L"Quit", Vector2(80, 70), m_spriteBatch, m_font, m_input, [this]
	{
		OnButtonPress();
	});

	m_currentSpriteTexture = m_textureManager->GetTexture("Assets/Textures/sprite_healthBar.png");
}

void Game::OnButtonPress()
{
	MessageBox(0, "Thanks for playing!", "Exiting Game", MB_OK);
	PostQuitMessage(0);
}

void Game::RefreshUI()
{
	if (m_player)
	{
		std::wstringstream ss;
		ss << "Score: " << floorf(m_player->GetScore()); //Concatenate
		m_scoreText = ss.str(); 
	}
}

void Game::InitGameWorld()
{
	// A GameBoard creates the world layout and manages the Tiles.
	// We pass it the Mesh and Texture managers as it will be creating tiles and walls
	m_gameBoard = new GameBoard(m_meshManager, m_textureManager, m_diffuseTexturedShader);

	// A player will select a random starting position.
	// We need to tell the player about the board it is standing on so it can validate movement
	// and ask the board what type of tile it is standing on.
	m_player = new Player(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
						  m_diffuseTexturedShader,
						  m_textureManager->GetTexture("Assets/Textures/tile_white.png"),
						  m_input,
						  m_gameBoard,
						  m_meshManager,
						  m_textureManager,
						  m_diffuseTexturedShader);
	for (int i = 0; i < monsterCount; i++)
	{
		m_monsters.push_back(new Monster(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
			m_diffuseTexturedShader,
			m_textureManager->GetTexture("Assets/Textures/tile_purple.png"),
			m_gameBoard,
			i + 1, //skill (determines how the monster moves and its health range)
			m_meshManager,
			m_textureManager,
			m_diffuseTexturedShader));
		m_monsters[i]->SetLookAt(m_player);
	}
}

void Game::Update(float timestep)
{
	// Look how short this function can be when we make objects responsible for their own logic.
	// Our only job out here is to Update the board and player, and check if the game is over.
	SetCursorPos((GetSystemMetrics(SM_CXSCREEN) / 2), (GetSystemMetrics(SM_CYSCREEN) / 2)); // CURSOR LOCK, NEEDS TO BE MADE TO ADAPT TO RESOLUTION AND WINDOW POSITIONING

	m_input->BeginUpdate();
	
	m_gameBoard->Update(timestep);
	m_player->Update(timestep, m_currentCam);

	for (int i = 0; i < m_monsters.size(); i++) //update all monsters
	{
		if (m_monsters[i]->IsAlive() == true)
		{
			m_monsters[i]->Update(timestep); //Update monster
			
			if (m_monsters[i]->GetBulletFlag() == true) //retrieve any new bullet objects from monsters
			{
				int startSlot = m_monsters[i]->GetSentBulletCounter(); //where in the monsters vector of bullets to start
				int stopSlot = m_monsters[i]->GetBulletSize();
				for (; startSlot < stopSlot; startSlot++)
				{
					m_monsterBullets.push_back(m_monsters[i]->GetBullets(startSlot));
					
				}
				m_monsters[i]->SetBulletFlag(false); //this loop won't be entered again until the monster shoots a new bullet
			}
		}
	}

	// The moves remaining bar doesn't need updating as it does nothing

	CheckGameOver(); //check if game end conditions have been met

	m_button->Update();
	RefreshUI();

	m_collisionManager->CheckCollisions(); //Check for collisions (Disable to minimise lag)

	// Sometimes creating a whole new child of Camera is a bit overkill. Here
	// we're just telling our existing camera what to do (it has been modified to include
	// the catch-up mode using LERP and also sets its look-at internally each frame).
	m_currentCam->SetTargetPosition(m_player->GetPosition());
	//m_currentCam->SetTargetPosition(m_player->GetPosition() + Vector3(0, 5, -5));
	m_currentCam->Update(timestep);
	
	m_input->EndUpdate();
}

void Game::Render()
{
	m_renderer->BeginScene(0.2f, 0.2f, 0.2f, 1.0f);

	// The board renders all of its tiles
	m_gameBoard->Render(m_renderer, m_currentCam);
	if (m_player->GetDoRender() == true) //for first person camera, can disable rendering player model
	{
		m_player->Render(m_renderer, m_currentCam);
	}

	m_player->RenderBullets(m_renderer, m_currentCam);

	//Render monsters
	for (int i = 0; i < m_monsters.size(); i++)
	{
		if (m_monsters[i]->IsAlive() == true)
		{
			m_monsters[i]->Render(m_renderer, m_currentCam);
			m_monsters[i]->RenderBullets(m_renderer, m_currentCam);
		}
	}
	//m_movesRemainingBar->Render(m_renderer, m_currentCam); //Hiding the moveRemainingBar for the purposes of the first person camera

	DrawUI();

	m_renderer->EndScene();		
}

void Game::DrawUI()
{
	m_renderer->SetCurrentShader(NULL);
	CommonStates states(m_renderer->GetDevice());
	m_spriteBatch->Begin(SpriteSortMode_Deferred, states.NonPremultiplied());

	m_button->Render();

	// Let's draw some text over our game
	m_fontLarge->DrawString(m_spriteBatch, m_scoreText.c_str(), Vector2(500, 650), Color(1.0f, 1.0f, 1.0f), 0, Vector2(0, 0));

	// Here's how we draw a sprite over our game
	//m_spriteBatch->Draw(m_currentSpriteTexture->GetShaderResourceView(), Vector2(20, 680), Color(1.0f, 1.0f, 1.0f));
	for (int HPsections = m_player->GetHealth() / 3; HPsections > 0; HPsections--) //Draw the HP bar using a section of bar for every 3 hp
	{
		m_spriteBatch->Draw(m_currentSpriteTexture->GetShaderResourceView(), Vector2(20 + (32*HPsections) , 680), Color(1.0f, 1.0f, 1.0f)); //32 pixels between sections
	}

	m_spriteBatch->End();
}

void Game::CheckGameOver()
{
	// Checks the condition that can end the game and informs the user

	const char* msg = "";

	if (m_player->GetHealth() <= 0.0f)
	{
		msg = "You've run out of health.";
	}
	else if (m_player->GetNumberOfMonstersDefeated() == monsterCount)
	{
		msg = "You've defeated all the monsters!";
	}
	if (msg != "")
	{
		std::stringstream ss;
		ss << msg << " You scored " << m_player->GetScore() << " and defeated " << m_player->GetNumberOfMonstersDefeated() << " monsters.";

		// Message Boxes are a blocking call which makes life a little easier here
		MessageBox(NULL, ss.str().c_str(), "Game Over", MB_OK);
		PostQuitMessage(0);

	}
}

void Game::Shutdown()
{
	if (m_player)
	{
		delete m_player;
		m_player = NULL;
	}

	if (m_gameBoard)
	{
		delete m_gameBoard;
		m_gameBoard = NULL;
	}

	if (m_movesRemainingBar)
	{
		delete m_movesRemainingBar;
		m_movesRemainingBar = NULL;
	}

	if (m_currentCam)
	{
		delete m_currentCam;
		m_currentCam = NULL;
	}

	if (m_meshManager)
	{
		m_meshManager->Release();
		delete m_meshManager;
		m_meshManager = NULL;
	}

	if (m_textureManager)
	{
		m_textureManager->Release();
		delete m_textureManager;
		m_textureManager = NULL;
	}

	if (m_diffuseTexturedShader)
	{
		m_diffuseTexturedShader->Release();
		delete m_diffuseTexturedShader;
		m_diffuseTexturedShader = NULL;
	}

	if (m_collisionManager)
	{
		delete m_collisionManager;
		m_collisionManager = NULL;
	}

	if (m_spriteBatch)
	{
		delete m_spriteBatch;
		m_spriteBatch = NULL;
	}

	if (m_font)
	{
		delete m_font;
		m_font = NULL;
	}

	if (m_fontLarge)
	{
		delete m_fontLarge;
		m_fontLarge = NULL;
	}

	if (m_button)
	{
		delete m_button;
		m_button = NULL;
	}
}