/*	FIT2096 - Assignment 1 Sample Solution
*	GameBoard.h
*	Created by Mike Yeates - 2017 - Monash University
*	Generates the board and manages a collection of Tiles.
*/

#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include "Tile.h"
#include "MeshManager.h"
#include "TextureManager.h"
#include <vector>

class GameBoard
{
private:
	MeshManager* m_meshManager;
	TextureManager* m_textureManager;
	Shader* m_tileShader;

	// How many tiles does this board manage
	const static int BOARD_WIDTH = 50;
	const static int BOARD_HEIGHT = 50;

	// Storing tiles in a 2D array to make neighbour checking easier
	Tile* m_tiles[BOARD_HEIGHT][BOARD_WIDTH];
	Tile* m_models[BOARD_HEIGHT][BOARD_WIDTH];

	std::vector<Tile*> m_tilesVector; //Also store the wall objects in a vector for collision detection

	void Generate();
	void AddWalls();
	void AddHealPacks(); //Adds the 3d floating health packs above healing tiles

public:
	GameBoard();
	GameBoard(MeshManager* meshManager, TextureManager* textureManager, Shader* tileShader);
	~GameBoard();

	void Update(float timestep);
	void Render(Direct3D* renderer, Camera* camera);

	void DeactivateTile(int x, int z);
	TileType GetTileTypeForPosition(int x, int z);
	Tile* GetRandomTileOfType(TileType type);
	std::vector<Tile*> GetTilesVector() { return m_tilesVector; }

};

#endif

