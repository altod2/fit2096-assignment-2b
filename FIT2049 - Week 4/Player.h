/*	FIT2096 - Assignment 1 Sample Solution
*	Player.h
*	Created by Mike Yeates - 2017 - Monash University
*	A Player listens to the keyboard and responds to arrow key presses.
*	It LERPs itself between cells and also asks the GameBoard where it can move.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"
#include "InputController.h"
#include "GameBoard.h"
#include "FirstPersonCamera.h"
#include "Collisions.h"
#include "PhysicsObject.h"
#include "Bullet.h"
#include "MeshManager.h"
#include "Shader.h"
#include "TextureManager.h"
#include "GameValues.h"

class Player : public PhysicsObject
{
private:
	// A Player should listen for its own input
	InputController* m_input;
	
	// Which board is the player currently on
	GameBoard* m_currentBoard;

	std::vector<Bullet*> m_bullets; // The bullets the player has shot
	std::vector<Bullet*>* m_bulletsPointer; //To pass to Game for CollisionManager constructor
	MeshManager* m_meshManager; //for creating the bullet objects
	TextureManager* m_textureManager;
	Shader* m_diffuseTexturedShader;

	// Game variables
	float m_moveSpeed;
	float m_health;
	int m_score;
	int m_monstersDefeated;
	bool m_isTrapped;
	bool m_doRender; //for the first person camera, if false disables the player mesh
	int m_shootCounter; //hacky way to make it so the players gun doesn't machine gun fire
	int m_fireRate; //rate at which player's gun fires (higher is slower)
	Vector3 m_localForward; //players local forward (to be passed to monster for movement type 4)

	// Check what type of tile is beneth us and react
	void ReactToTile(TileType targetTileType);

	// Used to spawn the player in a random position and teleport between blue tiles
	void TeleportToTileOfType(TileType type);

	// For taking damage
	void BeHit(int amount);

	// Collisions
	CBoundingBox m_boundingBox;

public:
	Player(Mesh* mesh, Shader* shader, Texture* texture, InputController* input, GameBoard* board, MeshManager* m_meshManager, TextureManager* m_textureManager, Shader* m_diffuseTexturedShader);
	~Player();

	void Update(float timestep); //Required so the class has an override for the virtual update function of gameobject, even if it doesn't get used
	void Update(float timestep, Camera* cam);

	void RenderBullets(Direct3D* renderer, Camera* camera);

	
	
	

	// Return the max and min damage the player can do respectively
	int GetMaxDamage() { return playerDamageMax; }
	int GetMinDamage() { return playerDamageMin; }

	// Game will use these to output info to the player
	
	
	std::vector<Bullet*>* GetBulletsVector() { return m_bulletsPointer; } //returns the bullet vector to pass to the collisionmanager constructor

	//Accessors
	float GetHealth() { return m_health; }
	int GetScore() { return m_score; } //Returns score for end of game and on screen display
	int GetNumberOfMonstersDefeated() { return m_monstersDefeated; } //Returns number of monsters defeated for end of game
	bool GetDoRender() { return m_doRender; }; //returns if the player should be rendered or not, for first person camera
	Vector3 GetLocalForward() { return m_localForward; } //For passing to Monster so movement type 4 can be implemented

	//Mutators
	void IncreaseScore(int amount) { m_score += amount; } //Increase the players score
	void IncreaseDefeated() { m_monstersDefeated++; } //Increase the monsters defeated counter by 1

	// Walking onto a tile/into a wall
	void OnTileCollisionEnter(Tile* other);
	void OnTileCollisionStay(Tile* other);
	void OnTileCollisionExit(Tile* other);

	// Walking into a monster
	void OnMonsterCollisionEnter();
	void OnMonsterCollisionStay();
	void OnMonsterCollisionExit();

	// Bullet hit on player
	void OnBulletCollisionEnter();
	void OnBulletCollisionStay();
	void OnBulletCollisionExit();

	CBoundingBox GetBounds() { return m_boundingBox; } // Returns bounding box for collisions
};

#endif
