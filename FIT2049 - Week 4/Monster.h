/*	FIT2096 - Assignment 1 Sample Solution
*	Monster.h
*	Created by Mike Yeates - 2017 - Monash University
*	A simpe object which has a health, can attack, and can be hit.
*/

#ifndef MONSTER_H
#define MONSTER_H

#include "GameBoard.h"
#include "GameValues.h"
#include "Player.h"

class Monster : public PhysicsObject
{
private:
	int m_health;
	int m_skill; //level of the monster
	float m_moveSpeed;
	bool m_isAlive;
	int m_shootCounter; //hacky way to make it so the monsters gun doesn't machine gun fire
	bool m_bulletFlag; //lets the game know there are new bullets to retrieve to pass to collisionmanager
	int m_sentBulletCounter; //slot that the bullet retriever is up to in the bullet vector

	//For the monster movement types
	bool m_atDestination; //for the monster that moves around randomly
	Tile* m_destinationTileA;//destination tile for case 3
	Tile* m_destinationTileB;//destination tile for case 5
	Vector3 m_randomTileVectorA; //vector to random tile for case 3
	Vector3 m_randomTileVectorB; //vector to random tile for case 5
	bool m_running; //for case 5

	GameBoard* m_currentBoard; // Which board is the monster currently on
	static Player* m_objectToLookAt; // The player object to be constantly facing
	std::vector<Bullet*> m_bullets; // The bullets the player has shot
	MeshManager* m_meshManager; //for creating the bullet objects
	TextureManager* m_textureManager;
	Shader* m_diffuseTexturedShader;

	// Collisions
	CBoundingBox m_boundingBox;

	// Used to spawn the monster in a random position and teleport between blue tiles
	void TeleportToTileOfType(TileType type);

public:
	Monster(bool boss);
	Monster(bool boss, Player* playerObj);
	Monster(Mesh* mesh, Shader* shader, Texture* texture, GameBoard* board, int skill, MeshManager* meshManager, TextureManager* textureManager, Shader* diffuseTexturedShader);
	~Monster();

	void Update(float timestep);

	int Attack();
	void BeHit(int amount);
	void SetLookAt(Player* playerobj) { m_objectToLookAt = playerobj; }
	bool IsAlive() { return m_isAlive; }
	int GetSkill() { return m_skill; }
	int GetMaxHealth() { return monsterHealthMax; }
	int GetMinHealth() { return monsterHealthMin; }
	Bullet* GetBullets(int i); //called by game, which has called GetSentBulletCounter() to know the slot to start at
	int GetSentBulletCounter() { return m_sentBulletCounter; }
	int GetBulletSize() { return m_bullets.size(); } //called by game to know what slot to stop retrieving bullets at
	bool GetBulletFlag() { return m_bulletFlag; }

	void SetBulletFlag(bool flag) { m_bulletFlag = flag; }

	void Shoot();
	void RenderBullets(Direct3D* renderer, Camera* camera);

	// Bullet hit on monster
	void OnBulletCollisionEnter();
	void OnBulletCollisionStay();
	void OnBulletCollisionExit();

	// Wall collision
	void OnTileCollisionEnter(Tile* other);
	void OnTileCollisionStay();
	void OnTileCollisionExit();

	CBoundingBox GetBounds() { return m_boundingBox; } // Returns bounding box for collisions
};

#endif