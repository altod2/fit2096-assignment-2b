#include "Player.h"
#include "Monster.h"
#include "MathsHelper.h"


Player::Player(Mesh* mesh, Shader* shader, Texture* texture, InputController* input, GameBoard* board, MeshManager* meshManager, TextureManager* textureManager, Shader* diffuseTexturedShader)
	: PhysicsObject(mesh, shader, texture, Vector3::Zero)
{
	// Values from GameValues.h
	m_moveSpeed = playerMoveSpeed;
	m_health = playerHealth;
	m_frictionAmount = playerFriction;
	m_fireRate = playerFireRate;

	m_input = input;
	m_currentBoard = board;

	// For creating the bullet objects
	m_meshManager = meshManager;
	m_textureManager = textureManager;
	m_diffuseTexturedShader = diffuseTexturedShader;
	
	// Set counters to 0
	m_score = 0;
	m_monstersDefeated = 0;
	m_shootCounter = 0;

	m_doRender = false; //T oggle for player model rendering, for first person camera
	
	m_bulletsPointer = &m_bullets; // For passing back to Game, which will then pass it to CollisionManager

	// Collision
	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

	TeleportToTileOfType(TileType::NORMAL); // Send the player to a random normal time on the game board
}

Player::~Player() 
{
	for (int i = 0; i < m_bullets.size(); i++)
	{
		delete m_bullets[i];
		m_bullets[i] = NULL;
	}
}

void Player::Update(float timestep) {} //Required to make the class non-virtual

void Player::Update(float timestep, Camera* cam)
{
	FirstPersonCamera* firstPCam = static_cast<FirstPersonCamera*>(cam); //Cast the camera we were passed to FirstPersonCamera so we can access its child members
	Vector3 worldForward = Vector3(0, 0, 1);
	Vector3 worldRight = Vector3(1, 0, 0);
	Matrix heading = Matrix::CreateRotationY(firstPCam->GetHeading()); //Create our heading from our camera
	m_localForward = Vector3::TransformNormal(worldForward, heading); //Set our local forward vector
	Vector3 localRight = Vector3::TransformNormal(worldRight, heading); //Set our local right vector

	// Keyboard Input
	if (m_input->GetKeyHold('W'))
	{
		ApplyForce(m_localForward * m_moveSpeed); //Movement calculation is handled by Physics object
	}
	if (m_input->GetKeyHold('S'))
	{
		ApplyForce(-m_localForward * m_moveSpeed);
	}
	if (m_input->GetKeyHold('A'))
	{
		ApplyForce(-localRight * m_moveSpeed);
	}
	if (m_input->GetKeyHold('D'))
	{
		ApplyForce(localRight * m_moveSpeed);
	}
	if (m_input->GetKeyHold(VK_ESCAPE)) //Exit the game if the escape key is pressed
	{
		MessageBox(0, "Thanks for playing!", "Exiting Game", MB_OK);
		PostQuitMessage(0);
	}
	
	// Mouse Input
	if (m_input->GetMouseDown(0))
	{
		//When shoot counter equals 0, the player fires, otherwise the counter is decremented by 1
		if (m_shootCounter <= 0)
		{
			m_bullets.push_back(new Bullet(m_meshManager->GetMesh("Assets/Meshes/bullet.obj"),
				m_diffuseTexturedShader,
				m_textureManager->GetTexture("Assets/Textures/bullet.png"),
				false,
				m_position,
				m_localForward,
				firstPCam->GetHeading()));

			m_shootCounter = m_fireRate; //Set counter back to fire rate
		}
		else
		{
			m_shootCounter -= 1; //Reduce the counter by one
		}
	}
	//Set the counter back to 0 so the player will always shoot as soon as they click rather than having to wait for the counter to build up
	//Disadvantage to this approach is that the player can speed up their rate of fire by rapidly clicking instead of clicking and holding
	if (m_input->GetMouseUp(0))
	{
		m_shootCounter = 0;
	}

	// Move the bounding box with the player
	m_boundingBox.SetMin(m_position + m_mesh->GetMin());
	m_boundingBox.SetMax(m_position + m_mesh->GetMax());

	// Update physics (movement)
	PhysicsObject::Update(timestep);
	
	// Update bullets
	for (int i = 0; i < m_bullets.size(); i++)
	{
		if (m_bullets[i]->GetEnabled() == true) //Only update if the bullet is enabled
		{
			m_bullets[i]->Update(timestep, m_position);
		}	
	}
	
	// Make sure the player never goes below 0 on the y axis
	if (m_position.y < 0.0f) { m_position.y = 0.0f; } 
}

void Player::RenderBullets(Direct3D* renderer, Camera* camera)
{
	for (int i = 0; i < m_bullets.size(); i++)
	{
		if (m_bullets[i]->GetEnabled() == true) //Only render if the bullet is enabled
		{
			m_bullets[i]->Render(renderer, camera);
		}
	}
}

void Player::ReactToTile(TileType targetTileType)
{
	switch (targetTileType)
	{
	case TileType::HEALTH:
		m_health += 5;
		break;
	case TileType::DAMAGE:
		m_health -= 10;
		break;
	case TileType::TELEPORT:
		TeleportToTileOfType(TileType::TELEPORT);
		break;
	default:
		break;
	}
}

void Player::TeleportToTileOfType(TileType type)
{
	Tile* destinationTile = m_currentBoard->GetRandomTileOfType(type);

	if (destinationTile)
	{
		m_position = destinationTile->GetPosition();
		m_position.y = 0.0f; //Make sure the player starts at y=0
	}
}

void Player::BeHit(int amount)
{
	// "abs" keeps a value positive
	m_health -= abs(amount);
}

// Tile collision reactions
void Player::OnTileCollisionEnter(Tile* other)
{
	OutputDebugString("Player Tile Collision Enter\n");
	if (other->GetType() == TileType::HEALTH)
	{
		ReactToTile(TileType::HEALTH);
		Vector3 pos = other->GetPosition(); //Get the postion of the tile we've collided with so we can disable it
		int x = pos.x;
		int z = pos.z;
		m_currentBoard->DeactivateTile(x, z);
	}
	else if (other->GetType() == TileType::DAMAGE)
	{
		ReactToTile(TileType::DAMAGE);
		Vector3 pos = other->GetPosition(); //Get the postion of the tile we've collided with so we can disable it
		int x = pos.x;
		int z = pos.z;
		m_currentBoard->DeactivateTile(x, z);
	}
	else if (other->GetType() == TileType::TELEPORT)
	{
		ReactToTile(TileType::TELEPORT);
		Vector3 pos = other->GetPosition(); //Get the postion of the tile we've collided with so we can disable it
		int x = pos.x;
		int z = pos.z;
		m_currentBoard->DeactivateTile(x, z);
	}
	else if (other->GetType() == TileType::WALL)
	{
		ApplyForce((m_position - other->GetPosition()) * 0.5f); //Knock the player back if they hit a wall
		OutputDebugString("Player Wall Collision Enter\n");
	}
	/* Disabled so the player dosen't get ping-ponged around
	else if (other->GetType() == TileType::DISABLED)
	{
		ApplyForce((m_position - other->GetPosition()) * 0.5f); //Knock the player back if they hit a wall
		OutputDebugString("Player Disabled Tile Collision Enter\n");
	}
	*/
}

void Player::OnTileCollisionStay(Tile* other)
{

}

void Player::OnTileCollisionExit(Tile* other)
{

}

// Monster collision reactions
void Player::OnMonsterCollisionEnter()
{
	OutputDebugString("Player Monster Collision Enter\n");
	m_health = 0.0f; //YOU DIED
}

void Player::OnMonsterCollisionStay()
{

}

void Player::OnMonsterCollisionExit()
{

}

// Bullet collision reactions
void Player::OnBulletCollisionEnter()
{
	OutputDebugString("Bullet Hit Player!\n");
	//Do damage to player
	BeHit(MathsHelper::RandomRange(monsterDamageMin, monsterDamageMax));
}

void Player::OnBulletCollisionStay()
{

}

void Player::OnBulletCollisionExit()
{

}
