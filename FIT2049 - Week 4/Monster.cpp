#include "Monster.h"
#include "MathsHelper.h"
#include "Direct3D.h"

Player* Monster::m_objectToLookAt = NULL; //Initalise the static member

Monster::Monster(bool boss) //redundant
{
	m_moveSpeed = 5.0f;
	m_currentBoard = NULL;
	m_health = 100.0f;

	if (boss == true)
	{
		m_health = 120;
		m_skill = MathsHelper::RandomRange(6, 13);
	}
	else
	{
		m_health = 100;
		m_skill = MathsHelper::RandomRange(3, 10);
	}


	m_isAlive = true;
}

Monster::Monster(bool boss, Player* playerObj) //redundant
{
	m_moveSpeed = 5.0f;
	m_currentBoard = NULL;
	m_health = 100.0f;
	m_objectToLookAt = playerObj;

	if (boss == true)
	{
		m_health = 120;
		m_skill = MathsHelper::RandomRange(6, 13);
	}
	else
	{
		m_health = 100;
		m_skill = MathsHelper::RandomRange(3, 10);
	}
	
	
	m_isAlive = true;
}

Monster::Monster(Mesh* mesh, Shader* shader, Texture* texture, GameBoard* board, int skill, MeshManager* meshManager, TextureManager* textureManager, Shader* diffuseTexturedShader)
	: PhysicsObject(mesh, shader, texture, Vector3::Zero)
{
	m_currentBoard = board;
	m_skill = skill;
	m_moveSpeed = monsterBaseMoveSpeed * m_skill;
	m_health = MathsHelper::RandomRange(GetMinHealth() * m_skill, GetMaxHealth() * m_skill);
	m_frictionAmount = monsterFriction;
	m_isAlive = true;
	m_shootCounter = 0;
	m_bulletFlag = false;
	m_sentBulletCounter = 0; //start at the first slot of the bullet vector
	m_atDestination = true;
	m_running = false;

	m_meshManager = meshManager; //for creating the bullet objects
	m_textureManager = textureManager;
	m_diffuseTexturedShader = diffuseTexturedShader;

	// Collision stuff
	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

	TeleportToTileOfType(TileType::NORMAL);
}

Monster::~Monster() 
{
	for (int i = 0; i < m_bullets.size(); i++)
	{
		delete m_bullets[i];
		m_bullets[i] = NULL;
	}
}

void Monster::Update(float timestep)
{
	Vector3 worldForward = Vector3(0, 0, 1);
	Vector3 worldRight = Vector3(1, 0, 0);

	//Monster movement patterns
	Vector3 playerVector = m_objectToLookAt->GetPosition() - m_position; //vector from monster to player (cases 1 + 2)
	playerVector.Normalize();
	Vector3 headOffDest = m_objectToLookAt->GetPosition() + (m_objectToLookAt->GetLocalForward() * Vector3(monsterHeadOffDist, 0.0f, monsterHeadOffDist)) - m_position; //destination for case 4
	switch (m_skill) {
	case 1: //constantly towards player
		ApplyForce(playerVector * m_moveSpeed);
		break;
	case 2: //constantly away from player
		ApplyForce(-playerVector * m_moveSpeed);
		break;
	case 3: //random points
		if (m_atDestination == true) //if it's at its destination it needs a new one
		{
			m_destinationTileA = m_currentBoard->GetRandomTileOfType(TileType::NORMAL);
			m_atDestination = false;
		}
		else
		{
			Vector3 destTilePos = m_destinationTileA->GetPosition(); //destination tile position including y co-ords
			Vector3 destTilePosXZ = Vector3(destTilePos.x, 0.0f, destTilePos.z); //destination tile position without y component
			m_randomTileVectorA = destTilePosXZ - m_position;
			m_randomTileVectorA.Normalize();
			ApplyForce(m_randomTileVectorA * m_moveSpeed);

			//Set atDestination if it has reached it's destination
			if (m_position.x >= (destTilePosXZ.x - 0.5f)
				&& m_position.x <= (destTilePosXZ.x + 0.5f)
				&& m_position.z >= (destTilePosXZ.z - 0.5f)
				&& m_position.z <= (destTilePosXZ.z + 0.5f))
			{
				m_atDestination = true;
			}
		}
		break;
	case 4: //head player off
		ApplyForce(headOffDest * m_moveSpeed);
		break;
	case 5: //remain still until player gets too close, then random point
		if (m_objectToLookAt->GetPosition().x >= (m_position.x - monsterScareRange) 
			&& m_objectToLookAt->GetPosition().x <= (m_position.x + monsterScareRange)
			&& m_objectToLookAt->GetPosition().z >= (m_position.z - monsterScareRange)
			&& m_objectToLookAt->GetPosition().z <= (m_position.z + monsterScareRange)
			&& m_running == false)
		{
			m_destinationTileB = m_currentBoard->GetRandomTileOfType(TileType::NORMAL);
			m_atDestination = false; //once it reaches its destination it will stop moving (stops it from bouncing around on the spot)
			m_running = true; //stops it from picking a new tile while still trying to get to its first choice
		}
		if (m_atDestination == false)
		{
			Vector3 destTilePos = m_destinationTileB->GetPosition(); //destination tile position including y co-ords
			Vector3 destTilePosXZ = Vector3(destTilePos.x, 0.0f, destTilePos.z); //destination tile position without y component
			m_randomTileVectorB = destTilePosXZ - m_position;
			m_randomTileVectorB.Normalize();
			ApplyForce(m_randomTileVectorB * m_moveSpeed);

			//Set atDestination if it has reached it's destination
			if (m_position.x >= (destTilePosXZ.x - 0.5f)
				&& m_position.x <= (destTilePosXZ.x + 0.5f)
				&& m_position.z >= (destTilePosXZ.z - 0.5f)
				&& m_position.z <= (destTilePosXZ.z + 0.5f))
			{
				m_atDestination = true;
				m_running = false;
			}
		}
		break;
	}

	// m_objectToLookAt must be set as the player object when the game is initialised
	// directionToPlayer = the vector from the Enemy to the player
	// calculated using vector subtraction (see lecture notes for week 2)
	Vector3 directionToPlayer = m_objectToLookAt->GetPosition() - m_position;
	// Normalize the vector to get a vector of unit length 
	directionToPlayer.Normalize();
	// Calculate the angle the enemy should be facing
	// There are a couple of ways to do this, atan2 is fairly straightforward
	m_rotY = atan2(directionToPlayer.x, directionToPlayer.z);

	// Move the bounding box with the monster
	m_boundingBox.SetMin(m_position + m_mesh->GetMin());
	m_boundingBox.SetMax(m_position + m_mesh->GetMax());

	Shoot(); // check if the monster will shoot, and do so if it does

	PhysicsObject::Update(timestep); //update physics

	 //Update bullets
	for (int i = 0; i < m_bullets.size(); i++)
	{
		if (m_bullets[i]->GetEnabled() == true) //only update if the bullet is enabled
		{
			m_bullets[i]->Update(timestep, m_position);
		}	
	}

	if (m_position.y < 0.0f) { m_position.y = 0.0f; } // Make sure the monster never goes below 0 on the y axis
}

int Monster::Attack() //redundant
{
	// A monster's attack power is limited to its skill
	return MathsHelper::RandomRange(0, m_skill);
}

void Monster::BeHit(int amount)
{
	// "abs" keeps a value positive
	m_health -= abs(amount);

	if (m_health <= 0)
	{
		m_isAlive = false;
		// Increment our score
		m_objectToLookAt->IncreaseScore(m_skill);
		m_objectToLookAt->IncreaseDefeated();
		OutputDebugString("Monster killed!!\n");
	}
}

void Monster::TeleportToTileOfType(TileType type)
{
	Tile* destinationTile = m_currentBoard->GetRandomTileOfType(type);

	if (destinationTile)
	{
		// We need to set both the current position and the target
		// The only time the player remains still is when these two positions match
		//m_targetPosition = destinationTile->GetPosition();
		m_position = destinationTile->GetPosition();

		// Tiles start up in the sky and fall down. Ensure player starts on the ground.
		//m_targetPosition.y = 0.0f;
		m_position.y = 0.0f;
	}
}

Bullet* Monster::GetBullets(int i)
{ 
	m_sentBulletCounter++;
	return m_bullets[i]; 
}

void Monster::Shoot()
{
	if (m_shootCounter <= 0) //hacky way to slow down the rate of fire
	{
		Vector3 worldForward = Vector3(0, 0, 1);
		Matrix heading = Matrix::CreateRotationY(m_rotY);
		Vector3 localForward = Vector3::TransformNormal(worldForward, heading); //forward relative to the monster
		Vector3 toPlayer = m_objectToLookAt->GetPosition() - m_position; //vector towards player from the monster

		m_bullets.push_back(new Bullet(m_meshManager->GetMesh("Assets/Meshes/bullet.obj"),
			m_diffuseTexturedShader,
			m_textureManager->GetTexture("Assets/Textures/bullet.png"),
			false,
			m_position,
			localForward,
			m_rotY));

		m_shootCounter = monsterFireRate + MathsHelper::RandomRange(0, 100); //set counter back to fire rate
		m_bulletFlag = true; //sets the flag to let the game know there are new bullets to retrieve to true
	}
	else
	{
		m_shootCounter -= 1;
	}	
}

void Monster::RenderBullets(Direct3D* renderer, Camera* camera)
{
	for (int i = 0; i < m_bullets.size(); i++)
	{
		if (m_bullets[i]->GetEnabled() == true)
		{
			m_bullets[i]->Render(renderer, camera);
		}
	}
}

// Bullet collision reactions
void Monster::OnBulletCollisionEnter()
{
	OutputDebugString("Bullet Hit!\n");
	BeHit(MathsHelper::RandomRange(m_objectToLookAt->GetMinDamage(), m_objectToLookAt->GetMaxDamage()));
	//Do damage to monster
}

void Monster::OnBulletCollisionStay()
{

}

void Monster::OnBulletCollisionExit()
{

}

// Tile collision reactions
void Monster::OnTileCollisionEnter(Tile* other)
{
	OutputDebugString("Monster Wall Hit\n");
	ApplyForce((m_position - other->GetPosition()) * 0.5f);
}

void Monster::OnTileCollisionStay()
{

}

void Monster::OnTileCollisionExit()
{

}