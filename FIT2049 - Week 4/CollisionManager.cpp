#include "CollisionManager.h"

CollisionManager::CollisionManager(std::vector<Tile*>* tiles, std::vector<Monster*>* monsters, std::vector<Player*>* players, std::vector<Bullet*>* playerBullets, std::vector<Bullet*>* monsterBullets)
{
	m_tiles = tiles;
	m_monsters = monsters;
	m_players = players;
	m_playerBullets = playerBullets;
	m_monsterBullets = monsterBullets;

	// Clear our arrays to 0 (NULL)
	memset(m_currentCollisions, 0, sizeof(m_currentCollisions));
	memset(m_previousCollisions, 0, sizeof(m_previousCollisions));

	m_nextCurrentCollisionSlot = 0;
}

void CollisionManager::CheckCollisions()
{
	// Check collision types
	PlayerToTile();
	MonsterToTile();
	PlayerToMonster();
	BulletToMonster();
	BulletToPlayer();

	// Move all current collisions into previous
	memcpy(m_previousCollisions, m_currentCollisions, sizeof(m_currentCollisions));

	// Clear out current collisions
	memset(m_currentCollisions, 0, sizeof(m_currentCollisions));
	
	// Now current collisions is empty, we'll start adding from the start again
	m_nextCurrentCollisionSlot = 0;

}

bool CollisionManager::ArrayContainsCollision(GameObject* arrayToSearch[], GameObject* first, GameObject* second)
{
	// See if these two GameObjects appear one after the other in specified collisions array
	// Stop one before length so we don't overrun as we'll be checking two elements per iteration
	for (int i = 0; i < MAX_ALLOWED_COLLISIONS - 1; i += 2)
	{
		if ((arrayToSearch[i] == first && arrayToSearch[i + 1] == second) ||
			arrayToSearch[i] == second && arrayToSearch[i + 1] == first)
		{
			// Found them!
			return true;
		}
	}

	// These objects were not colliding last frame
	return false;
}

void CollisionManager::AddCollision(GameObject* first, GameObject* second)
{	
	// Add the two colliding objects to the current collisions array
	// We keep track of the next free slot so no searching is required
	m_currentCollisions[m_nextCurrentCollisionSlot] = first;
	m_currentCollisions[m_nextCurrentCollisionSlot + 1] = second;
	
	m_nextCurrentCollisionSlot += 2;
}

void CollisionManager::PlayerToTile()
{
	// We'll check each kart against every item box
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_players->size(); i++)
	{
		for (unsigned int j = 0; j < m_tiles->size(); j++)
		{
			
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Player* player = (*m_players)[i];
			Tile* tile = (*m_tiles)[j];
			if (tile->GetType() != TileType::NORMAL && tile->GetType() != TileType::DISABLED) //only run the next section if the tile type is one a collision matters for, for runtime performance
			{
				// Fetch bounding box from the current kart and item box
				CBoundingBox playerBounds = player->GetBounds();
				CBoundingBox tileBounds = tile->GetBounds();

				// Are they colliding this frame?
				bool isColliding = CheckCollision(playerBounds, tileBounds);

				// Were they colliding last frame?
				bool wasColliding = ArrayContainsCollision(m_previousCollisions, player, tile);
				if (isColliding)
				{
					// Register the collision
					AddCollision(player, tile);

					if (wasColliding)
					{
						// We are colliding this frame and we were also colliding last frame - that's a collision stay
						// Tell the player it is colliding with an wall (collision stay)
						//wall->OnPlayerCollisionStay();
						player->OnTileCollisionStay(tile);
					}
					else
					{
						// We are colliding this frame and we weren't last frame - that's a collision enter
						// Tell the player it has begun colliding with an wall (collision enter)
						//wall->OnCharacterCollisionEnter();
						player->OnTileCollisionEnter(tile);
					}
				}
				else
				{
					if (wasColliding)
					{
						// We aren't colliding this frame but we were last frame - that's a collision exit
						// Tell the player it is no longer colliding with an wall (collision exit)
						//wall->OnCharacterCollisionExit();
						player->OnTileCollisionExit(tile);
					}
				}
			}
		}
	}
}

void CollisionManager::MonsterToTile()
{
	// We'll check each kart against every item box
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_monsters->size(); i++)
	{
		for (unsigned int j = 0; j < m_tiles->size(); j++)
		{

			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Monster* monster = (*m_monsters)[i];
			Tile* tile = (*m_tiles)[j];
			if (tile->GetType() == TileType::WALL) //only run the next section if the tile type is one a collision matters for, for runtime performance
			{
				// Fetch bounding box from the current kart and item box
				CBoundingBox monsterBounds = monster->GetBounds();
				CBoundingBox tileBounds = tile->GetBounds();

				// Are they colliding this frame?
				bool isColliding = CheckCollision(monsterBounds, tileBounds);

				// Were they colliding last frame?
				bool wasColliding = ArrayContainsCollision(m_previousCollisions, monster, tile);
				if (isColliding)
				{
					// Register the collision
					AddCollision(monster, tile);

					if (wasColliding)
					{
						// We are colliding this frame and we were also colliding last frame - that's a collision stay
						// Tell the player it is colliding with an wall (collision stay)
						//wall->OnPlayerCollisionStay();
						monster->OnTileCollisionStay();
					}
					else
					{
						// We are colliding this frame and we weren't last frame - that's a collision enter
						// Tell the player it has begun colliding with an wall (collision enter)
						//wall->OnCharacterCollisionEnter();
						monster->OnTileCollisionEnter(tile);
					}
				}
				else
				{
					if (wasColliding)
					{
						// We aren't colliding this frame but we were last frame - that's a collision exit
						// Tell the player it is no longer colliding with an wall (collision exit)
						//wall->OnCharacterCollisionExit();
						monster->OnTileCollisionExit();
					}
				}
			}
		}
	}
}

void CollisionManager::PlayerToMonster()
{
	// We'll check each kart against every item box
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_players->size(); i++)
	{
		for (unsigned int j = 0; j < m_monsters->size(); j++)
		{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Player* player = (*m_players)[i];
			Monster* monster = (*m_monsters)[j];
			if (monster->IsAlive() != false)
			{
				// Fetch bounding box from the current kart and item box
				CBoundingBox playerBounds = player->GetBounds();
				CBoundingBox monsterBounds = monster->GetBounds();

				// Are they colliding this frame?
				bool isColliding = CheckCollision(playerBounds, monsterBounds);

				// Were they colliding last frame?
				bool wasColliding = ArrayContainsCollision(m_previousCollisions, player, monster);
				if (isColliding)
				{
					// Register the collision
					AddCollision(player, monster);

					if (wasColliding)
					{
						// We are colliding this frame and we were also colliding last frame - that's a collision stay
						// Tell the player it is colliding with an wall (collision stay)
						//wall->OnPlayerCollisionStay();
						player->OnMonsterCollisionStay();
					}
					else
					{
						// We are colliding this frame and we weren't last frame - that's a collision enter
						// Tell the player it has begun colliding with an wall (collision enter)
						//wall->OnCharacterCollisionEnter();
						player->OnMonsterCollisionEnter();
					}
				}
				else
				{
					if (wasColliding)
					{
						// We aren't colliding this frame but we were last frame - that's a collision exit
						// Tell the player it is no longer colliding with an wall (collision exit)
						//wall->OnCharacterCollisionExit();
						player->OnMonsterCollisionExit();
					}
				}
			}
		}
	}
}

void CollisionManager::BulletToMonster()
{
	// We'll check each kart against every item box
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_playerBullets->size(); i++)
	{
		for (unsigned int j = 0; j < m_monsters->size(); j++)
		{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Bullet* bullet = (*m_playerBullets)[i];
			Monster* monster = (*m_monsters)[j];
			if (monster->IsAlive() != false)
			{
				// Fetch bounding box from the current kart and item box
				CBoundingBox bulletBounds = bullet->GetBounds();
				CBoundingBox monsterBounds = monster->GetBounds();

				// Are they colliding this frame?
				bool isColliding = CheckCollision(bulletBounds, monsterBounds);

				// Were they colliding last frame?
				bool wasColliding = ArrayContainsCollision(m_previousCollisions, bullet, monster);
				if (isColliding)
				{
					// Register the collision
					AddCollision(bullet, monster);

					if (wasColliding)
					{
						// We are colliding this frame and we were also colliding last frame - that's a collision stay
						// Tell the player it is colliding with an wall (collision stay)
						monster->OnBulletCollisionStay();
						bullet->OnMonsterCollisionStay();
					}
					else
					{
						// We are colliding this frame and we weren't last frame - that's a collision enter
						// Tell the player it has begun colliding with an wall (collision enter)
						monster->OnBulletCollisionEnter();
						bullet->OnMonsterCollisionEnter();
					}
				}
				else
				{
					if (wasColliding)
					{
						// We aren't colliding this frame but we were last frame - that's a collision exit
						// Tell the player it is no longer colliding with an wall (collision exit)
						monster->OnBulletCollisionExit();
						bullet->OnMonsterCollisionExit();
					}
				}
			}
		}
	}
}

void CollisionManager::BulletToPlayer()
{
	// We'll check each kart against every item box
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_players->size(); i++)
	{
		for (unsigned int j = 0; j < m_monsterBullets->size(); j++)
		{

			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Player* player = (*m_players)[i];
			Bullet* bullet = (*m_monsterBullets)[j];

			// Fetch bounding box from the current kart and item box
			CBoundingBox playerBounds = player->GetBounds();
			CBoundingBox bulletBounds = bullet->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(playerBounds, bulletBounds);

			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, bullet, player);
			if (isColliding)
			{
				// Register the collision
				AddCollision(bullet, player);

				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the player it is colliding with an wall (collision stay)
					bullet->OnPlayerCollisionStay();
					player->OnBulletCollisionStay();
				}
				else
				{
					// We are colliding this frame and we weren't last frame - that's a collision enter
					// Tell the player it has begun colliding with an wall (collision enter)
					bullet->OnPlayerCollisionEnter();
					player->OnBulletCollisionEnter();
				}
			}
			else
			{
				if (wasColliding)
				{
					// We aren't colliding this frame but we were last frame - that's a collision exit
					// Tell the player it is no longer colliding with an wall (collision exit)
					bullet->OnPlayerCollisionExit();
					player->OnBulletCollisionExit();
					
				}
			}
		}
	}
}