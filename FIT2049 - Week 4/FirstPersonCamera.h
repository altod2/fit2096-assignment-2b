#ifndef FIRSTPERSONCAMERA_H
#define FIRSTPERSONCAMERA_H

#include "Camera.h"
#include "GameObject.h"
#include "InputController.h"

class FirstPersonCamera :
	public Camera
{
private:
	InputController* m_input;
	GameObject* m_playerObject;

	bool m_catchupMode;
	float m_catchupSpeed;

	float m_rotationSpeed;

	float m_heading;
	float m_pitch;

public:
	FirstPersonCamera();
	FirstPersonCamera(InputController* input, GameObject* player, bool catchup, float catchupSpeed);

	void Update(float timestep);

	float GetHeading() { return m_heading; }
	float GetPitch() { return m_pitch; }

};

#endif
