/*	FIT2096 - Example Code
*	Game.h
*	Created by Elliott Wilson & Mike Yeates - 2016 - Monash University
*	This class is the heart of our game and is also where our game logic will reside
*	It contains the overall Update and Render method for the whole game
*	You should eventually split some game logic into other objects which Game will manage
*	Watch the size of this class - it can grow out of control very fast!
*/

#ifndef GAME_H
#define GAME_H

#include "Direct3D.h"
#include "Camera.h"
#include "FirstPersonCamera.h"
#include "InputController.h"
#include "MeshManager.h"
#include "TextureManager.h"
#include "GameObject.h"
#include "GameBoard.h"
#include "Player.h"
#include "Monster.h"
#include "Button.h"
#include "CollisionManager.h"
#include "Bullet.h"
#include <vector>

#include "DirectXTK/SpriteBatch.h"
#include "DirectXTK/SpriteFont.h"

class Game
{
private:
	Direct3D* m_renderer;
	InputController* m_input;
	Camera* m_currentCam;		
	MeshManager* m_meshManager;
	TextureManager* m_textureManager;
	Shader* m_diffuseTexturedShader;

	// Our game data.
	GameBoard* m_gameBoard;
	Player* m_player;
	GameObject* m_movesRemainingBar;
	CollisionManager* m_collisionManager;

	//Vectors for passing to CollisionManager
	std::vector<Tile*> m_tiles;
	std::vector<Player*> m_players;
	std::vector<Monster*> m_monsters;
	std::vector<Bullet*>* m_playerBullets;
	std::vector<Bullet*> m_monsterBullets;
	
	SpriteBatch* m_spriteBatch;
	SpriteFont* m_font;
	SpriteFont* m_fontLarge;

	Texture* m_currentSpriteTexture;
	std::wstring m_scoreText;
	Button* m_button;

	// Splitting initialisation up into several steps
	bool InitShaders();
	bool LoadMeshes();
	bool LoadTextures();
	void InitGameWorld();
	void LoadFonts();
	void InitUI();
	void OnButtonPress();
	void RefreshUI();
	void DrawUI();

	void CheckGameOver();

public:
	Game();	
	~Game();

	bool Initialise(Direct3D* renderer, InputController* input); //The initialise method will load all of the content for the game (meshes, textures, etc.)

	void Update(float timestep);	//The overall Update method for the game. All gameplay logic will be done somewhere within this method
	void Render();					//The overall Render method for the game. Here all of the meshes that need to be drawn will be drawn

	void Shutdown(); //Cleanup everything we initialised
};

#endif