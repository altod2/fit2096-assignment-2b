#include "Bullet.h"


Bullet::Bullet(Mesh* mesh, Shader* shader, Texture* texture, bool hostile, Vector3 startPos, Vector3 localForward, float rotY)
	: GameObject(mesh, shader, texture, (startPos + Vector3(0.0f, 1.0f, 0.0f)))
{
	m_hostile = hostile;
	m_moveSpeed = bulletMoveSpeed;
	m_travelDistance = bulletTravelDist;
	m_targetPosition = startPos + Vector3(0.0f, 1.0f, 0.0f);
	m_localForward = localForward;
	m_rotY = rotY;
	m_enabled = true;
}


Bullet::~Bullet() 
{
}

void Bullet::Update(float timestep) {} //so the class has an override for the virtual update function of gameobject

void Bullet::Update(float timestep, Vector3 shooterPosition)
{
	// Disable bullets if they are to far away
	Vector3 vDistance = m_position - shooterPosition; //Vector of the bullets position relative to the monster that fired it
	float distance = vDistance.Length(); //How far away the bullet is as a float value
	if (distance > m_travelDistance)
	{
		SetEnabled(false);
	}

	m_targetPosition += m_localForward * m_moveSpeed; //set the position to move to
	m_position = Vector3::Lerp(m_position, m_targetPosition, (timestep * m_moveSpeed)); //move towards the updated postion

	// Move the bounding box with the bullet
	m_boundingBox.SetMin(m_position + m_mesh->GetMin());
	m_boundingBox.SetMax(m_position + m_mesh->GetMax());
}

// Hitting a monster
void Bullet::OnMonsterCollisionEnter()
{
	OutputDebugString("Bullet Disabled!\n");
	//disable bullet
	SetEnabled(false);
}

void Bullet::OnMonsterCollisionStay()
{
	
}

void Bullet::OnMonsterCollisionExit()
{

}

// Hitting a player
void Bullet::OnPlayerCollisionEnter()
{
	OutputDebugString("Bullet Disabled!\n");
	//disable bullet
	SetEnabled(false);
}

void Bullet::OnPlayerCollisionStay()
{
	
}

void Bullet::OnPlayerCollisionExit()
{

}